package com.tutorial.crud.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.tutorial.crud.entity.Tarea;
import com.tutorial.crud.security.entity.Usuario;

public interface ITareaDao  extends JpaRepository<Tarea, Long>{

	@Query("from Usuario")
	public List<Usuario> findAllUsuarios();
}
