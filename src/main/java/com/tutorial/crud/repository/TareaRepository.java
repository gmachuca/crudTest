package com.tutorial.crud.repository;

import com.tutorial.crud.entity.Tarea;
import com.tutorial.crud.security.entity.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TareaRepository extends JpaRepository<Tarea, Integer> {
	@Query("from Usuario")
	public List<Usuario> findAllUsuarios();
}
