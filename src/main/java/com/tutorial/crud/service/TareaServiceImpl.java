package com.tutorial.crud.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tutorial.crud.entity.Tarea;
import com.tutorial.crud.security.entity.Usuario;
import com.tutorial.crud.security.repository.ITareaDao;

@Service
public class TareaServiceImpl implements ITareaService {

	@Autowired
	private ITareaDao clienteDao;

	@Override
	@Transactional(readOnly = true)
	public List<Tarea> findAll() {
		return (List<Tarea>) clienteDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Tarea> findAll(Pageable pageable) {
		return clienteDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Tarea findById(Long id) {
		return clienteDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Tarea save(Tarea cliente) {
		return clienteDao.save(cliente);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		clienteDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAllUsuarios() {
		return clienteDao.findAllUsuarios();
	}

}
