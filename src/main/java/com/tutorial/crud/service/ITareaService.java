package com.tutorial.crud.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.tutorial.crud.entity.Tarea;
import com.tutorial.crud.security.entity.Usuario;

public interface ITareaService {

	public List<Tarea> findAll();
	
	public Page<Tarea> findAll(Pageable pageable);
	
	public Tarea findById(Long id);
	
	public Tarea save(Tarea cliente);
	
	public void delete(Long id);
	
	public List<Usuario> findAllUsuarios();

}