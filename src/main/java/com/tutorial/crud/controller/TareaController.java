package com.tutorial.crud.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tutorial.crud.entity.Tarea;
import com.tutorial.crud.security.entity.Usuario;
import com.tutorial.crud.service.ITareaService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/tarea")
@CrossOrigin(origins = "*")
public class TareaController {

	@Autowired
	private ITareaService tareaService;
	

	
	// private final Logger log = LoggerFactory.getLogger(TareaRestController.class);
	@ApiOperation("Muestra una lista de tareas existentes")
	@GetMapping("/tareas")
	public List<Tarea> index() {
		return tareaService.findAll();
	}
	@ApiOperation("Muestra una lista de tareas existentes paginado")
	@GetMapping("/tareas/page/{page}")
	public Page<Tarea> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return tareaService.findAll(pageable);
	}
	
	@ApiOperation("Muestra la tarea por Id")
	@GetMapping("/tareas/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		
		Tarea tarea = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			tarea = tareaService.findById(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(tarea == null) {
			response.put("mensaje", "El tarea ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Tarea>(tarea, HttpStatus.OK);
	}
	@ApiOperation("Creacion de tareas")
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/tareas")
	public ResponseEntity<?> create(@Valid @RequestBody Tarea tarea, BindingResult result) {
		
		Tarea tareaNew = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			tarea.setCreateAt(new Date());
			tareaNew = tareaService.save(tarea);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El tarea ha sido creado con éxito!");
		response.put("tarea", tareaNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	@ApiOperation("Modificacion de Tarea por ID")
	 @PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/tareas/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Tarea tarea, BindingResult result, @PathVariable Long id) {

		Tarea tareaActual = tareaService.findById(id);

		Tarea tareaUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if (tareaActual == null) {
			response.put("mensaje", "Error: no se pudo editar, el tarea ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			tareaActual.setTitulo(tarea.getTitulo());
			tareaActual.setDescripcion(tarea.getDescripcion());
			tareaActual.setEstado(tarea.getEstado());
			tareaActual.setCreateAt(tarea.getCreateAt());
			//tareaActual.setRegion(tarea.getRegion());

			tareaUpdated = tareaService.save(tareaActual);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el tarea en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El tarea ha sido actualizado con éxito!");
		response.put("tarea", tareaUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	@ApiOperation("Eliminacion de tarea")
	 @PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/tareas/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		Map<String, Object> response = new HashMap<>();
		
		try {
			Tarea tarea = tareaService.findById(id);			
		    tareaService.delete(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar el tarea de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El tarea eliminado con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@ApiOperation("Lista de Usuarios a Asignar")
	@GetMapping("/usuarios")
	public List<Usuario> listarRegiones(){
		return tareaService.findAllUsuarios();
	}
}
